class User < ApplicationRecord
  has_many :participants
  has_many :posts
  has_many :post_likes
  has_secure_password

  include Paperclip::Glue
  has_attached_file :avatar, 
                    styles: {large: "300x300>" ,medium: "164x164>", thumb: "100x100>" }, 
                    default_url: "/assets/default_avatar.png", 
                    content_type: ["image/jpeg", "image/gif", "image/png"]
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/
  validates_with AttachmentSizeValidator, attributes: :avatar, less_than: 2.megabytes

end
