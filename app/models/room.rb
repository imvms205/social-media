class Room < ApplicationRecord
  has_many :participants
  has_many :messages
  # Participant.joins(:room)
  #            .where(rooms:{is_group:false})
  #            .select("count(user_id),room_id")
  #            .group(:room_id).where("user_id in (?)",[5,6])
  #            .having("count(user_id) = 2 ")
end
