class Message < ApplicationRecord
  # enum status: { public: "public", friendOnly: "friendOnly", onlyMe: "onlyMe" }, _prefix: :status
  enum status: { sending: 0, sent: 1, received: 2, seen: 3 }
  belongs_to :user, class_name: "User", foreign_key: "sent_id"
  belongs_to :room
end
