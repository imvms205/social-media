class Post < ApplicationRecord
  belongs_to :user
  has_many :post_likes, dependent: :destroy
  has_many :users_who_like, through: :post_likes, source: :user
  has_many :comments
  enum status: { to_public: 0, to_friends: 1, to_me: 2 }

  include Paperclip::Glue
  has_attached_file :avatar, 
                    styles: {large: "300x300>" ,medium: "164x164>", thumb: "100x100>" }, 
                    content_type: ["image/jpeg", "image/gif", "image/png"]
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/
  validates_with AttachmentSizeValidator, attributes: :avatar, less_than: 2.megabytes
end
