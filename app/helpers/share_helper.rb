module ShareHelper
  def current_user
    if !cookies.key?("token")
      return nil
    end
    User.find(JsonWebToken.decode(cookies[:token])[:id])
  end

  def chat_bar_data
    friends = Friend.where(send_id: current_user.id, status: true)

    @users_not_have_room = []

    friends.each do |f|
      user = User.find(f.receive_id)
      @users_not_have_room << user
    end

    @users_and_room = []

    @users_not_have_room.each do |user|
      room_id = Participant.joins(:room).where(rooms: { is_group: false }).select("count(user_id),room_id").where("user_id in (?)", [current_user.id, user.id]).group(:room_id).having("count(user_id) = 2 ")[0]&.room_id
      puts "room_id : #{room_id}"
      count_message = Room.find(room_id).messages.count
      @users_and_room << { user: user, room_id: room_id, count_message: count_message }
    end
    @users_and_room.sort! { |a, b| b[:count_message] <=> a[:count_message] }
  end

  def post_data(posts)
    @posts_and_user = []
    posts.each do |p|
      user = User.find(p.user_id)
      user_id = user.id
      boolean = Post.find(p.id).users_who_like.exists?(id: current_user.id)
      post_id = p.id

      comments_and_users = []
      comments = Comment.where(post_id: post_id).where("comment_id IS ?", nil).order("created_at desc").limit(2).reverse
      comment_left = Comment.where(post_id: post_id).where("comment_id IS ?", nil).count - 2
      comments.each do |c|
        user = User.find(c.user_id)
        like_count = c.like_count
        liked = Comment.find(c.id).users_who_like.exists?(id: current_user.id)
        replies = Comment.where(comment_id: c.id)
        replies_and_users = []
        replies.each do |r|
          reply = r
          like_count = r.like_count
          liked = Comment.find(r.id).users_who_like.exists?(id: current_user.id)
          user = reply.user
          replies_and_users << { reply: reply, user: user, like_count: like_count, liked: liked }
        end
        comments_and_users << { comment: c, user: user, like_count: like_count, liked: liked, replies_and_users: replies_and_users }
      end
      @posts_and_user << { post: p, user: user, boolean: boolean, comments_and_users: comments_and_users, comment_left: comment_left }
    end
  end
end
