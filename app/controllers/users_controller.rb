class UsersController < ApplicationController
  before_action :not_logged, only: [:home]
  before_action :logged, only: [:signup, :signin]

  def signup
    render :layout => "applicationNotLogin"
  end

  def created
    create_test = params.permit(:phone, :email)
    test = User.find_by(create_test)
    if test.nil?
      user = User.new(user_params)
      if user.save
        redirect_to "/signin"
      else flash[:error] = "create user false"
        render "signup"       end
    else
      render "signup"
    end
  end

  def signin
    render :layout => "applicationNotLogin"
  end

  def home
    friends = Friend.where(send_id: current_user.id)
    friends_id = []
    friends.each do |f|
      friends_id << f.receive_id
    end
    friends_id << current_user.id

    posts = Post.where("user_id in (?) ", friends_id).where(status: ["to_public", "to_friends"]).order("created_at desc")

    post_data posts
    chat_bar_data
  end

  def login
    cookies[:token] = JsonWebToken.encode(id: 5)
  end

  def loginn
    cookies[:token] = JsonWebToken.encode(id: 6)
  end

  def loginnn
    cookies[:token] = JsonWebToken.encode(id: 1)
  end

  def logout
    cookies.delete :token
  end

  def not_logged
    if !cookies.key?("token") || JsonWebToken.decode(cookies[:token]).nil? || !User.exists?(id: JsonWebToken.decode(cookies[:token])[:id])
      redirect_to "/signin"
    end
  end

  def logged
    if cookies.key?("token") &&
       !JsonWebToken.decode(cookies[:token]).nil? &&
       User.exists?(id: JsonWebToken.decode(cookies[:token])[:id])
      redirect_to "/home"
    end
  end

  def current_user_profile_page
    posts = Post.where(user_id: current_user.id).order("created_at desc")
    post_data posts
    chat_bar_data
    @current_user_friend_list = Friend.where(send_id: current_user.id, status: true)
    @current_user_friend_count = @current_user_friend_list.count
    @current_user_friend_to_show = @current_user_friend_list.limit(9)

    @allFriendOfCurrentUser = []

    @current_user_friend_to_show.each do |friend|
      @friend = User.find(friend.receive_id)
      @allFriendOfCurrentUser.push(@friend)
    end
  end

  def update_avatar
    if !current_user.nil?
      current_user.update(avatar: params[:avatar])
      puts "------------------------UPDATE AVATAR SUCCESS---------------------------"
    end
  end

  def settings_page
    chat_bar_data
    current_user
  end

  def update_profile
    current_user.update(user_params)
    flash[:success] = "Update profile success"
  end

  private

  def user_params
    params.permit(:name, :phone, :email, :password)
  end

  def avatar_params
    params.require(:user).permit(:avatar)
  end
end
