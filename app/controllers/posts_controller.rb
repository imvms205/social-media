class PostsController < ApplicationController
  def createPost
    content = params[:content]
    status = params[:status]
    if params[:avatar] =='undefined'
      new_post = Post.create(content: params[:content], user_id: current_user.id, status: params[:status])
    else
      new_post = Post.create(content: params[:content], user_id: current_user.id, status: params[:status], avatar: params[:avatar])
    end
    puts "-------------------------new_post.content : #{new_post}"
    render json: { post: new_post,avatar:new_post.avatar.url(:medium),avatar_exist:!new_post.avatar_file_name.nil? }
  end

  def getPosts
    posts = Post.all
    render json: { posts: posts }
  end

  def changeStatusPost
    post_id = params[:post_id].to_i
    status = params[:status]
    Post.find(post_id).update_attribute(:status, status)
    head :ok  
  end

  def modifyPost
    post_id = params[:post_id].to_i
    post = Post.find(post_id)
    post.update(post_params)
  end

  def deletePost
    post_id = params[:post_id]
    Post.find(post_id).destroy
    head :ok
  end

  def likePost
    post_id = params[:post_id]
    if !PostLike.exists?(post_id: post_id, user_id: current_user.id)
      Post.find(post_id).post_likes << PostLike.new(user_id: current_user.id, post_id: post_id)
      Post.find(post_id).increment!(:like_count)
    else
      Post.find(post_id).post_likes.find_by(user_id: current_user.id).destroy
      Post.find(post_id).decrement!(:like_count)
    end
    post = Post.find(post_id)
    like_count = post.like_count
    render json: { like_count: like_count }
  end

  def getPostLike
    post_id = params[:post_id]
    post = Post.find(post_id)
    like_count = post.like_count
    users = post.users_who_like
    render json: { like_count: like_count, users: users }
  end

  private

  def post_params
    params.require(:post).permit(:content, :status, :image, :video, :like_count)
  end
end
