class MessagesController < ApplicationController
  def getMessage
    current_user_id = current_user.id
    friend_id = params["friend_id"]
    room_id = params["room_id"]
    page = params["page"]
    friend = User.find_by_id(friend_id)
    return if friend.nil?
    return if Room.find_by(id: room_id).nil?
    correct_room = Room.find_by(id: room_id).participants.where(user_id: current_user_id)
    return if correct_room.length < 1
    messages = Room.find(room_id).messages.order("id desc").limit(10 * page).reverse
    if (10 * page - 10 >= Room.find(room_id).messages.count)
      render json: { messages: messages, friend: friend, max: true }
    else
      render json: { messages: messages, friend: friend }
    end
  end

  def getMessageIcon
    friend_room_message = []
    page = params["page"]

    messages = ap Message.joins("INNER JOIN rooms ON rooms.id=messages.room_id").joins("INNER JOIN participants ON rooms.id=participants.room_id").where("participants.user_id=?", current_user.id).select("messages.room_id,max(messages.id) as id ").group(:room_id).limit(10 * page).order("max(messages.id) desc")
    messages_no_limit = ap Message.joins("INNER JOIN rooms ON rooms.id=messages.room_id").joins("INNER JOIN participants ON rooms.id=participants.room_id").where("participants.user_id=?", current_user.id).select("messages.room_id,max(messages.id) as id ").group(:room_id).order("max(messages.id) desc")
    count = Message.joins("INNER JOIN rooms ON rooms.id=messages.room_id").joins("INNER JOIN participants ON rooms.id=participants.room_id").where("participants.user_id=?", current_user.id).select("messages.room_id,max(messages.id) as id ").group(:room_id).length
    count_is_not_known = 0
    messages_no_limit.each do |m|
      room_id = m.room_id
      room = Room.find(room_id)
      friend_id = Room.find(room_id).participants.where.not(user_id: current_user.id)[0].user_id
      friend = User.find(friend_id)
      message = Message.find(m.id)
      if (message.is_known == false && message.sent_id != current_user.id)
        count_is_not_known += 1
      end
    end
    messages.each do |m|
      room_id = m.room_id
      room = Room.find(room_id)
      friend_id = Room.find(room_id).participants.where.not(user_id: current_user.id)[0].user_id
      friend = User.find(friend_id)
      message = Message.find(m.id)
      friend_room_message << {
        friend: friend,
        room: room,
        message: message,
      }
    end
    
    if (10 * page - 10 >= count)
      render json: { friend_room_message: friend_room_message, max: true, count_is_not_known: count_is_not_known }
    else
      render json: { friend_room_message: friend_room_message, count_is_not_known: count_is_not_known }
    end
  end

  def knowAllMessages
    messages_no_limit = ap Message.joins("INNER JOIN rooms ON rooms.id=messages.room_id").joins("INNER JOIN participants ON rooms.id=participants.room_id").where("participants.user_id=?", current_user.id).select("messages.room_id,max(messages.id) as id ").group(:room_id).order("max(messages.id) desc")
    messages_no_limit.each do |m|
      room_id = m.room_id
      room = Room.find(room_id)
      friend_id = Room.find(room_id).participants.where.not(user_id: current_user.id)[0].user_id
      friend = User.find(friend_id)
      message = Message.find(m.id)
      if (message.is_known == false && message.sent_id != current_user.id)
        message.update_attribute(:is_known, true)
      end
    end
    head :ok
  end

  def createMessage
    base_url = "https://fakebook-491fd.firebaseio.com/"
    firebase = Firebase::Client.new(base_url)
    room_id = params["room_id"]
    current_user_id = current_user.id
    message = params["message"]
    return if Room.find_by(id: room_id).nil?
    correct_room = Room.find_by(id: room_id).participants.where(user_id: current_user_id)
    return if correct_room.length < 1

    Room.find(room_id).messages << Message.new(content: message, sent_id: current_user_id)

    participants = Room.find(room_id).participants

    ids = []

    participants.each do |p|
      ids << p.user_id
    end
    ids.each do |id|
      response = firebase.push("users/#{id}/rooms/#{room_id}", {
        sent_id: current_user.id,
        room_id: room_id.to_i,
        is_seen: 0,
        :'.priority' => 1,
      })
    end
    head :ok
  end

  def seenAt
    room_id = params["room_id"].to_i
    sent_id = params["sent_id"].to_i
    receive_id = params["receive_id"].to_i
    last_message_id = params["last_message_id"].to_i

    Time.zone = "Asia/Bangkok"
    time = Time.current

    Message.find_by_id(last_message_id).update_attribute(:seen_at, time)
    Message.find_by_id(last_message_id).update_attribute(:is_known, true)
    base_url = "https://fakebook-491fd.firebaseio.com/"
    firebase = Firebase::Client.new(base_url)
    response = firebase.push("users/#{receive_id}/rooms/#{room_id}", {
      sent_id: sent_id,
      room_id: room_id,
      is_seen: 1,
      :'.priority' => 1,
    })

    head :ok
  end
end
