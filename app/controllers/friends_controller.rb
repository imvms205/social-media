class FriendsController < ApplicationController
  def not_friend
    if current_user.id == params[:id].to_i
      redirect_to "/users"
    else
      is_friend = Friend.exists?(send_id: current_user.id, receive_id: params[:id].to_i, status: true)

      if is_friend
        posts = Post.where(user_id: 6).where(status: ["to_public", "to_friends"]).order("created_at desc")
      else
        posts = Post.where(user_id: params[:id].to_i).where(status: ["to_public"]).order("created_at desc")
      end

      post_data posts
      chat_bar_data
      @user = User.find(params[:id])
      @name_only = @user&.name.strip.split(/\s+/).last

      @friend_list = Friend.where(send_id: params[:id], status: true)
      @friend_count = @friend_list.count
      puts "---------------------------------+++++++#{@friend_count}"
      @friend_to_show = @friend_list.limit(9)
      @friendShowList = []
      @friend_to_show.each do |fr|
        @friend = User.find(fr.receive_id)
        @friendShowList.push(@friend)
      end

      @friend_request = Friend.where(send_id: current_user&.id, receive_id: @user.id).first
      @friend_respond = Friend.where(send_id: @user.id, receive_id: current_user&.id).first
      @friend_room_message = []
      messages = ap Message.joins("INNER JOIN rooms ON rooms.id=messages.room_id").joins("INNER JOIN participants ON rooms.id=participants.room_id").where("participants.user_id=?", current_user.id).select("messages.room_id,max(messages.id) as id ").group(:room_id).limit(9).order("max(messages.id) desc")

      messages.each do |m|
        room_id = m.room_id
        room = Room.find(room_id)
        friend_id = Room.find(room_id).participants.where.not(user_id: current_user.id)[0].user_id
        friend = User.find(friend_id)
        message = Message.find(m.id)
        @friend_room_message << {
          friend: friend,
          room: room,
          message: message,
        }
      end

      if @friend_request.nil? && @friend_respond.nil?
        @friend = 0  #0 = NOT friend
        @follow = Follow.where(follower_id: current_user&.id, followed_id: @user.id).first
        if !@follow.nil?
          @following = true
        else
          @following = false
        end
        return
      else
        if !@friend_request.nil? && @friend_request.status == false
          @friend = 1  #1 = current user SEND friend request
          return
        else
          if !@friend_respond.nil? && @friend_respond.status == false
            @friend = 2 #2 = current user RECEIVE friend request
            return
          else
            if (!@friend_request.nil? && @friend_request.status == true) || (!@friend_respond.nil? && @friend_respond.status == true)
              render "friend"
              return
            end
          end
        end
      end
    end
  end

  def friendRequestPost
    send_id = params["send_id"].to_i
    receive_id = params["receive_id"].to_i
    @friendCheck = Friend.where(send_id: send_id, receive_id: receive_id).first
    if @friendCheck.nil?
      # create friend request
      @friend = Friend.new(send_id: send_id, receive_id: receive_id)
      if @friend.save
        # push to FireBase
        base_url = "https://fakebook-491fd.firebaseio.com/"
        firebase = Firebase::Client.new(base_url)
        response = firebase.push("friends/receive_id/#{receive_id}", {
          send_id: send_id,
        })
        # create follow from current user to target user
        follow = Follow.create(follower_id: send_id, followed_id: receive_id)
      else
        p "CANNOT SAVE FRIEND REQUEST !!!!"
      end
    else
      p "Friend request Already exists"
      return
    end
  end

  def fr_request_received
    # id of user send friend request
    send_id = params["friend_request_from_user_id"].to_i
    fr_request_count = Friend.where(receive_id: current_user.id, status: false, is_seen: false).count
    render json: { friend_request: true, fr_request_count: fr_request_count }
  end

  def friend_respond
    send_id = params["send_id"].to_i
    receive_id = params["receive_id"].to_i
    accept = params["accept"]
    @friend_request = Friend.where(send_id: receive_id, receive_id: send_id).first
    @follow_request = Follow.where(follower_id: receive_id, followed_id: send_id).first
    if accept == true
      return if Friend.exists?(send_id: receive_id, receive_id: send_id, status: true, is_seen: true)
      Friend.create(send_id: send_id, receive_id: receive_id, status: true, is_seen: true)
      @friend_request.update(status: true, is_seen: true)
      Follow.create(follower_id: send_id, followed_id: receive_id)
      room = Room.create()
      room.participants << Participant.new(user_id: receive_id)
      room.participants << Participant.new(user_id: send_id)
      p "-----YOU ARE FRIEND NOW"
    else
      @friend_request.destroy
      @follow_request.destroy
      p "-----Friend Request has been deleted"
    end
  end

  def cancel_friend_request
    send_id = params["send_id"].to_i
    receive_id = params["receive_id"].to_i
    @follow = Follow.where(follower_id: send_id, followed_id: receive_id).first
    @friendCheck = Friend.where(send_id: send_id, receive_id: receive_id).first
    if !@friendCheck.nil?
      # create friend request
      @friendCheck&.destroy
      # push to FireBase
      base_url = "https://fakebook-491fd.firebaseio.com/"
      firebase = Firebase::Client.new(base_url)
      response = firebase.push("friends/target_cancel/#{receive_id}", {
        cancel_request_from_id: send_id,
      })
      # create follow from current user to target user
      @follow&.destroy
    else
      p "-----CANNOT CANCEL FRIEND REQUEST !!!!"
      p "------YOU HAVE NOT SENT ANY FRIEND REQUEST "
      return
    end
  end

  def cancel_friend_firebase
    cancel_from_user_id = params["cancel_request_from_user_id"].to_i
    render json: { cancel_request: true }
  end

  def unfriend
    send_id = params["send_id"].to_i
    receive_id = params["receive_id"].to_i
    Friend.where(send_id: send_id, receive_id: receive_id).first.destroy
    Friend.where(send_id: receive_id, receive_id: send_id).first.destroy
    @follow = Follow.where(follower_id: send_id, followed_id: receive_id).first.destroy
  end

  def unfollow
    send_id = params["send_id"].to_i
    receive_id = params["receive_id"].to_i
    Follow.where(follower_id: send_id, followed_id: receive_id).first.destroy
  end
end
