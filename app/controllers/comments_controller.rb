class CommentsController < ApplicationController
  def createComment
    post_id = params[:post_id]
    content = params[:content]
    newComment = Comment.create(content: content, user_id: current_user.id, post_id: post_id)
    Post.find(post_id).increment!(:comment_count)
    render json: { id: newComment.id, created_at: newComment.created_at }
  end

  def createReply
    post_id = params[:post_id]
    comment_id = params[:comment_id]
    content = params[:content]
    newReply = Comment.create(content: content, user_id: current_user.id, post_id: post_id, comment_id: comment_id)
    render json: { id: newReply.id, created_at: newReply.created_at }
  end

  def getMoreComments
    post_id = params[:post_id]
    current_comment_num = params[:current_comment_num]
    first_comment_id = params[:first_comment_id]

    comments_and_users = []
    comments = Comment.where(post_id: post_id).where("id < ? ", first_comment_id).order("created_at desc").limit(10).reverse
    comment_left = Comment.where(post_id: post_id).where("id < ? ", first_comment_id).where("comment_id IS ?", nil).count - 10
    comments.each do |c|
      user = User.find(c.user_id)
      like_count = c.like_count
      liked = Comment.find(c.id).users_who_like.exists?(id: current_user.id)
      replies = Comment.where(comment_id: c.id)
      replies_and_users = []
      replies.each do |r|
        reply = r
        like_count = r.like_count
        liked = Comment.find(r.id).users_who_like.exists?(id: current_user.id)
        user = reply.user
        replies_and_users << { reply: reply, user: user, like_count: like_count, liked: liked }
      end
      comments_and_users << { comment: c, user: user, like_count: like_count, liked: liked, replies_and_users: replies_and_users }
    end

    render json: { comments_and_users: comments_and_users, comment_left: comment_left }
  end

  def deleteComment
    comment_id = params[:comment_id]
    comment = Comment.find(comment_id)
    comment.post.decrement!(:comment_count)
    comment.destroy
    head :ok
  end

  def modifyComment
    comment_id = params[:comment_id]
    content = params[:content]
    Comment.find(comment_id).update(content: content)
    head :ok
  end

  def likeComment
    comment_id = params[:comment_id]

    if !CommentLike.exists?(comment_id: comment_id, user_id: current_user.id)
      Comment.find(comment_id).comment_likes << CommentLike.new(user_id: current_user.id, comment_id: comment_id)
      Comment.find(comment_id).increment!(:like_count)
    else
      Comment.find(comment_id).comment_likes.find_by(user_id: current_user.id).destroy
      Comment.find(comment_id).decrement!(:like_count)
    end

    like_count = Comment.find(comment_id).like_count

    render json: { like_count: like_count }
  end

  def getCommentLike
    comment_id = params[:comment_id]
    comment = Comment.find(comment_id)
    like_count = comment.like_count
    users = comment.users_who_like
    render json: { like_count: like_count, users: users }
  end
end
