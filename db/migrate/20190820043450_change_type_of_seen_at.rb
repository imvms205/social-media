class ChangeTypeOfSeenAt < ActiveRecord::Migration[6.0]
  def change
    change_column :messages, :seen_at, :datetime
  end
end
