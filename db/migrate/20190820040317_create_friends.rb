class CreateFriends < ActiveRecord::Migration[6.0]
  def change
    create_table :friends do |t|
      t.integer :send_id
      t.integer :receive_id
      t.boolean :status

      t.timestamps
    end
  end
end
