class AddColumnAvatarToPost < ActiveRecord::Migration[6.0]
  def up
    add_attachment :posts, :avatar
  end

  def down
    remove_attachment :posts, :avatar
  end
end
