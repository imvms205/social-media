class AddCommentCountToPosts < ActiveRecord::Migration[6.0]
  def change
    add_column :posts, :comment_count, :integer, :default => 0
    change_column :posts, :like_count, :integer, :default => 0
    #Ex:- add_column("admin_users", "username", :string, :limit =>25, :after => "email")
  end
end
