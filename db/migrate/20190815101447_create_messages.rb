class CreateMessages < ActiveRecord::Migration[6.0]
  def change
    create_table :messages do |t|
      t.integer :send_id
      t.integer :room_id
      t.string :content
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
