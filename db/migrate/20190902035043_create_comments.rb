class CreateComments < ActiveRecord::Migration[6.0]
  def change
    create_table :comments do |t|
      t.integer :user_id
      t.string :content
      t.integer :post_id
      t.integer :comment_id
      t.integer :like_count

      t.timestamps
    end
  end
end
