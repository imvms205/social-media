class AddSeenAtMessages < ActiveRecord::Migration[6.0]
  def change
    add_column :messages, :seen_at, :string
  end
end
