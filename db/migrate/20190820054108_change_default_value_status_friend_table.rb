class ChangeDefaultValueStatusFriendTable < ActiveRecord::Migration[6.0]
  def change
    change_column :friends, :status, :boolean, :default => false
  end
end
