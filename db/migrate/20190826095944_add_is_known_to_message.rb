class AddIsKnownToMessage < ActiveRecord::Migration[6.0]
  def change
    add_column :messages, :is_known, :boolean, :default => false
  end
end
