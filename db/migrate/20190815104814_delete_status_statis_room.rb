class DeleteStatusStatisRoom < ActiveRecord::Migration[6.0]
  def change
    remove_column :rooms, :statis
    remove_column :rooms, :status
  end
end
