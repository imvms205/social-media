class AddColumnIsSeenToFriendTable < ActiveRecord::Migration[6.0]
  def change
    add_column :friends, :is_seen, :boolean, :default => false
  end
end
