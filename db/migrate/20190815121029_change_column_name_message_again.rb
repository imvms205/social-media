class ChangeColumnNameMessageAgain < ActiveRecord::Migration[6.0]
  def change
    remove_column :messages, :send_id
    add_column :messages, :sent_id, :integer
  end
end
