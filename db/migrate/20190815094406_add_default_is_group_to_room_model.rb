class AddDefaultIsGroupToRoomModel < ActiveRecord::Migration[6.0]
  def change
    change_column :rooms, :is_group, :boolean, default: false
  end
end
