class ChangeAgainColumnOfComment < ActiveRecord::Migration[6.0]
  def change
    change_column :comments, :like_count, :integer, :default => 0
    #Ex:- change_column("admin_users", "email", :string, :limit =>25)
  end
end
