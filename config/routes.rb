Rails.application.routes.draw do
  root "users#signup"

  get "/signup", to: "users#signup"
  post "/signup", to: "users#created"

  get "/signin", to: "users#signin"
  get "/home", to: "users#home"
  get "/users", to: "users#current_user_profile_page"
  post "/users", to: "users#update_avatar"

  get "/users/settings", to: "users#settings_page"
  post "/users/settings", to: "users#update_profile"

  post "/login", to: "users#login"
  post "/loginn", to: "users#loginn"
  post "/loginnn", to: "users#loginnn"

  post "/logout", to: "users#logout"
  post "/getMessage", to: "messages#getMessage"
  post "/getMessageIcon", to: "messages#getMessageIcon"
  get "/knowAllMessages", to: "messages#knowAllMessages"
  post "/createMessage", to: "messages#createMessage"

  post "/seenAt", to: "messages#seenAt"

  get "/friend/:id", to: "friends#not_friend"
  post "/friend/:id", to: "friends#friendRequestPost"

  get "/friend_request", to: "friends#fr_request_received"
  post "/friend_request", to: "friends#fr_request_received"

  post "/friend_respond", to: "friends#friend_respond"

  post "/cancel_friend_request", to: "friends#cancel_friend_request"
  post "/cancel_friend_firebase", to: "friends#cancel_friend_firebase"

  post "/unfriend", to: "friends#unfriend"
  post "/unfollow", to: "friends#unfollow"

  post "createPost", to: "posts#createPost"
  get "getPosts", to: "posts#getPosts"
  post "changeStatusPost", to: "posts#changeStatusPost"
  post "modifyPost", to: "posts#modifyPost"
  post "deletePost", to: "posts#deletePost"
  post "likePost", to: "posts#likePost"
  post "getPostLike", to: "posts#getPostLike"

  post "createComment", to: "comments#createComment"
  post "getMoreComments", to: "comments#getMoreComments"
  post "deleteComment", to: "comments#deleteComment"
  post "modifyComment", to: "comments#modifyComment"
  post "likeComment", to: "comments#likeComment"
  post "getCommentLike", to: "comments#getCommentLike"

  post "createReply", to: "comments#createReply"
end
